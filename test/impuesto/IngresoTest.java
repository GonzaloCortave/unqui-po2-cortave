package impuesto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IngresoTest {
	private Ingreso ingreso;
	private Ingreso ingreso2;
	private IngresoHorasExtra ingresoHorasExtra;
	
	@BeforeEach
	public void setUp(){
		ingreso = new Ingreso("Febrero", "Trabajo herreria", 300d);
		ingreso2 = new Ingreso("Enero", "Trabajo carpinteria", 200d);
		ingresoHorasExtra = new IngresoHorasExtra("Marzo", "Pintura", 100d, 30);
	}
	@Test
	public void testConstructorIngreso() {
		assertEquals(ingreso.getMesDePercepcion(), "Febrero");
		assertEquals(ingreso2.getMesDePercepcion(), "Enero");
		assertEquals(ingresoHorasExtra.getMesDePercepcion(), "Marzo");
		
		assertEquals(ingreso.getConcepto(), "Trabajo herreria");
		assertEquals(ingreso2.getConcepto(), "Trabajo carpinteria");
		assertEquals(ingresoHorasExtra.getConcepto(), "Pintura");
		
		assertEquals(ingreso.getMontoPercibido(), 300d);
		assertEquals(ingreso2.getMontoPercibido(), 200d);
		assertEquals(ingresoHorasExtra.getMontoPercibido(), 100d);
		
		assertEquals(ingresoHorasExtra.getHorasExtra(), 30);
		
	}
	@Test
	public void testMontoImponible() {
		assertEquals(300d,ingreso.getMontoImponible());
		assertEquals(200d,ingreso2.getMontoImponible());
		assertEquals(0d,ingresoHorasExtra.getMontoImponible());
	}
}
