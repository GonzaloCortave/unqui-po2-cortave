package impuesto;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TrabajadorTest {
	
	Trabajador trabajador;
	private Ingreso ingreso;
	private Ingreso ingreso2;
	private IngresoHorasExtra ingresoHorasExtra;
	
	@BeforeEach
	public void setUp() {
		
		trabajador = new Trabajador();

		ingreso = new Ingreso("Febrero", "Trabajo herreria", 300d);
		ingreso2 = new Ingreso("Enero", "Trabajo carpinteria", 200d);
		ingresoHorasExtra = new IngresoHorasExtra("Marzo", "Pintura", 100d, 30);		
		
		trabajador.agregarIngreso(ingreso);
		trabajador.agregarIngreso(ingreso2);
		trabajador.agregarIngreso(ingresoHorasExtra);
	}

	@Test
	public void testAgregadoDeIngresos() {
		assertEquals(3,trabajador.getIngresos().size());
	}

	
	@Test
    public void testTotalPercibidoDelTrabajador() {
        assertEquals( 600d,trabajador.getTotalPercibido() );
    }

    @Test
    public void testMontoImponible() {
        assertEquals( 500d,trabajador.getMontoImponible() );
    }

    @Test
    public void testImpuestoAPagar() {
        assertEquals( 10d,trabajador.getImpuestoAPagar() );
    }
}

