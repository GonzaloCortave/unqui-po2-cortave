package unq;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.po2.tp3.Multioperador;

public class MultioperadorTestCase {

	private Multioperador multioperador;
	
	@Before
	public void setUp() throws Exception{
		
		multioperador = new Multioperador();
		
		multioperador.addNumber(1);
		multioperador.addNumber(2);
		multioperador.addNumber(2);
		multioperador.addNumber(3);
		multioperador.addNumber(1);
		multioperador.addNumber(1);
		multioperador.addNumber(1);
		
	}
	
	@Test
	public void testRestaDeTodosLosNumeros() {
		int amount = multioperador.getRestaDeTodosLosNumeros();
		assertEquals(amount, -11);
	}
	
	@Test
	public void testSumaDeTodosLosNumeros() {
		int amount = multioperador.getSumaDeTodosLosNumeros();
		assertEquals(amount, 11);
	}
	
	@Test
	
	public void testMultilicacionDeTodosLosNumeros() {
		int amount = multioperador.getMultiplicacionDeTodosLosNumeros();
		assertEquals(amount, 12);
	}

}
