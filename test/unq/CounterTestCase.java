package unq;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
 
public class CounterTestCase {    
    private Counter counter;    
    
    /**
     * Crea un escenario de test b�sico, que consiste en un contador 
     * con 10 enteros
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
   	 
   	 //Se crea el contador
   	 counter = new Counter();
   	 
   	 //Se agregan los numeros. Un solo par y nueve impares
   	 counter.addNumber(1);
   	 counter.addNumber(3);
   	 counter.addNumber(5);
   	 counter.addNumber(7);
   	 counter.addNumber(9);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(4);
    }
 
    /**
     * Verifica la cantidad de pares
     */
    @Test
    public void testCantidadNumerosPares() {
   	 
   	 // Getting the even occurrences
   		 int amount = counter.getCantidadPares();
   			 
   	 // I check the amount is the expected one
   		 assertEquals(amount, 1);
    }
   
    @Test
    public void testCantidadNumerosImpares() {
    	
    	int amount = counter.getCantidadImpares();
    	
    	assertEquals(amount, 9);
    }
    
    @Test
    public void testCantidadDeMultiplos() {
    	
    	int amount = counter.getCantidadMultiplos(3);
    	
    	assertEquals(amount, 2);
    }
    
    @Test
    public void testInt() {
    	assertEquals(counter.valorDeInt(), 0);
    }
    
    @Test
    public void testInteger() {
    	assertEquals(counter.valorInteger(), null);
    }
}
