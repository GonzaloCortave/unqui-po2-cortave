package unq;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.po2.tp3.Punto;

public class PuntoTestCase {
	Punto point;
	@Before
	public void setUp() throws Exception {
		point = new Punto(1,2);
	}

	
	@Test
	public void testCuandoAUnPuntoSeLeSumaOtroPunto() {
		Punto puntoEsperado;
		puntoEsperado = point.sumarPuntoNuevaInstancia(new Punto(2,3));
		float xn = puntoEsperado.getX();
		float yn = puntoEsperado.getY();
		assertEquals(xn,2);
		assertEquals(yn,4);
	}

}
