package mercado;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoTest {
	private Producto producto;
	private ProductoDeCooperativa productoCoop;
	
	@BeforeEach
	public void setUp() {
		producto = new Producto("Manzana", 10.0d, 20);
		productoCoop = new ProductoDeCooperativa("Banana", 10.0d, 10);
	}

	@Test
	public void testConstructorDeProducto() {
		assertEquals("Manzana", producto.getNombre());
		assertEquals(10.0d, producto.getPrecio());
		assertEquals(20, producto.getStock());
	}
	@Test
	public void testAlDecrementarStock() {
		producto.decrementarStock();
		assertEquals(19, producto.getStock());
		producto.decrementarStock();
		producto.decrementarStock();
		assertEquals(17, producto.getStock());
	}
	@Test
	public void testCuandoSeRegistraElProductoDevuelveElPrecioYDecrementaElStock() {
		producto.registrarse();
		assertEquals(10, producto.registrarse());
		assertEquals(18, producto.getStock());
		
	}
	@Test 
	public void testElPrecioDelProductoDeCooperativaEsUn10PorcientoMenosDelDeLista() {
		assertEquals(9, productoCoop.getPrecio());
	}
}
