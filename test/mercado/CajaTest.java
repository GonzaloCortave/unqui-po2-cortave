package mercado;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CajaTest {
	private Caja caja;
	private Producto producto;
	private ProductoDeCooperativa productoCoop;
	
	@BeforeEach
	public void setUp() {
		caja = new Caja();
		producto = new Producto("Banana",20.0d,10);
		productoCoop = new ProductoDeCooperativa("Banana", 10.0d, 10);
	}
	@Test
	void testCuandoSeCreaUnaCajaRegistradoraElMontoAPagarEsDe0() {
		assertEquals(0, caja.getMontoTotal());;
	}
	@Test
	void testElMontoTotalDeLaCajaEsLaSumaDeLosProductosRegistrados() {
		caja.registrar(producto);
		caja.registrar(productoCoop);
		assertEquals(29, caja.getMontoTotal());
	}
	@Test
	void testAlPedirElMontoTotalDeLaCajaRegistradoraSeVuelveASetearEn0() {
		caja.registrar(producto);
		caja.registrar(producto);
		assertEquals(40, caja.getMontoTotal());
		assertEquals(0, caja.getMontoTotal());
		caja.registrar(producto);
		caja.registrar(producto);
		caja.registrar(producto);
		caja.registrar(producto);
		assertEquals(80, caja.getMontoTotal());
		assertEquals(0, caja.getMontoTotal());
		
	}
}
