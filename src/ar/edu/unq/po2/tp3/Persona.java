package ar.edu.unq.po2.tp3;

import java.time.LocalDate;

public class Persona {
	
	private String nombre;
	private LocalDate nacimiento;
	private String apellido;
	
	public Persona(String nombre, LocalDate nacimiento, String apellido) {
		this.setNombre(nombre);
		this.setNacimiento(nacimiento);
		this.setApellido(apellido);
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;	
	}
	
	public String apellido() {
		return apellido;
	}
	public int edad() {
		LocalDate now = LocalDate.now();
		int age = now.getYear() - nacimiento.getYear();
		return age;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(LocalDate nacimiento) {
		this.nacimiento = nacimiento;
	}
	public boolean menorQue(Persona persona) {
		return this.edad() < persona.edad();
	}
	
}
