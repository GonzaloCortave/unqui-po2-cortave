package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class EquipoDeTrabajo {
	private String nombre;
	private ArrayList<Persona> integrantes = new ArrayList<Persona>();
	
	public EquipoDeTrabajo(String nombre) {
		super();
		this.setNombre(nombre);
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void agregarAlEquipo(Persona persona) {
		this.integrantes.add(persona);
	}
	public int cantidaDeIntegrantes() {
		return this.integrantes.size();
	}
	public float promedioEdadIntegrantes() {
		int sumaDeEdades = 0;
		for (Persona persona : this.integrantes) {
			sumaDeEdades = sumaDeEdades + persona.edad();
		}
		return sumaDeEdades/this.cantidaDeIntegrantes();
	}
	
	
}
