package ar.edu.unq.po2.tp3;

import java.util.ArrayList;

public class Multioperador {
	
	ArrayList<Integer> intArray =  new ArrayList<Integer>();
	
	public void addNumber(int number) {
		this.intArray.add(number);
	}
	
	public int getRestaDeTodosLosNumeros() {
		int counter = 0;
		for(int number : this.intArray) {
			counter = counter - number;
		}
		return counter;
	}

	public int getSumaDeTodosLosNumeros() {
		int counter = 0;
		for(int number : this.intArray) {
			counter = counter + number;
		}
		return counter;
	}

	public int getMultiplicacionDeTodosLosNumeros() {
		int counter = 1;
		for(int number : this.intArray) {
			counter = counter * number;
		}
		return counter;
	}

}