package ar.edu.unq.po2.tp3;

public class Punto {
	
	private float x;
	private float y;
	
	public Punto(float x, float y) {
		super();
		this.setXY(x, y);
	}
	
	public void setXY(float x, float y) {
		
		this.setX(x);
		this.setY(y); 
		
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public Punto sumarPuntoNuevaInstancia(Punto punto) {
		Punto puntoEsperado;
		float xn = punto.getX() + this.getX();
		float yn = punto.getY() + this.getY();
		puntoEsperado = new Punto(xn, yn);
		
		return puntoEsperado;
	
	}public Punto sumarPunto (Punto punto) {
		
		float xn = punto.getX() + this.getX();
		float yn = punto.getY() + this.getY();
		this.setXY(xn, yn);
		
		return this;
	}
	
	
	
}
