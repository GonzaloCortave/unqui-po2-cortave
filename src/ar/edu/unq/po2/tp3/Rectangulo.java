package ar.edu.unq.po2.tp3;
public class Rectangulo {
	/**
	 * Proposito: Crea un rectangulo segun su ubicacion, su alto y su ancho. 
	 * Su ubicacion es su esquina superior izquierda.
	 */
	private Punto esquinaSuperiorIzq;
	private int alto;
	private int ancho;	
	
	public Rectangulo(Punto punto,int alto,int ancho) {
		this.ubicarRectangulo(punto);
		this.setAncho(ancho);
		this.setAlto(alto);
	}

	private void ubicarRectangulo(Punto punto) {
		esquinaSuperiorIzq = punto;
	
	}

	private void setAlto(int alto2) {
		alto = alto2;
		
	}

	private void setAncho(int ancho2) {
		ancho = ancho2;
		
	}
	public float getArea() {
		float area = alto * ancho;
		return area;
	}
	public float getPerimetro() {
		float perimetro = (alto * 2)+(ancho * 2);
		return perimetro;
	}
	public boolean esHorizontal() {
		return alto < ancho;
	}
	public boolean esVertical() {
		return alto > ancho;
	}
}
