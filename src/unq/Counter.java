package unq;

import java.util.ArrayList;

public class Counter {

	ArrayList<Integer> arrayDeNumeros = new ArrayList<Integer>();
	
	public void addNumber(Integer numero) {
		this.arrayDeNumeros.add(numero);
	}

	public int getPares() {
		
		int cantidadDePares = 0;
		for (int number : this.arrayDeNumeros) {
			if(this.esPar(number)) {
				cantidadDePares = cantidadDePares + 1;
			}
		}
		return cantidadDePares;
	}

	private boolean esPar(int number) {
		
		if (number%2==0) {
			return true;
		}else { return false;
		}
	}
	public int getImpares() {
		
		int cantidadDeImpares = 0;
		for (int number : this.arrayDeNumeros) {
			if(this.esImpar(number)) {
				cantidadDeImpares = cantidadDeImpares + 1;
			}
		}
		return cantidadDeImpares;
	}
	private boolean esImpar(int number) {
		
		return !this.esPar(number);
	}

	public int getMultiplos(int n2) {
		
		int cantidadDeMultiplos = 0;
		for (int number : this.arrayDeNumeros) {
			if(esMultiplo(number, n2)) {
				cantidadDeMultiplos = cantidadDeMultiplos + 1;
			}
		}
		return cantidadDeMultiplos;
	}
	
	public static boolean esMultiplo(int n1,int n2){
		if (n1%n2==0) {
			return true;
		}else {
			return false;
		}
	}
	
}
