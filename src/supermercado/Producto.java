package supermercado;

public class Producto {
	private String nombre;
	private double precio;
	private boolean esPrecioCuidado;

	public Producto(String nombre, double precio, boolean b) {
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = b;
	}

	public Producto(String nombre, double precio) {
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = false;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public boolean esPrecioCuidado() {
		return esPrecioCuidado;
	}

	public void aumentarPrecio(double valorAAumentar) {
		this.precio  = precio + valorAAumentar;
	}


}
