package supermercado;

public class ProductoPrimeraNecesidad extends Producto {
	
	private int descuento;

	public ProductoPrimeraNecesidad(String nombreP, double precio, boolean b, int descuento) {
		super(nombreP,precio,b);
		this.descuento = descuento;
		
	}
	public ProductoPrimeraNecesidad(String nombreP, double precio, int descuento) {
		super(nombreP,precio);
		this.descuento = descuento;
	}
	

	@Override
	public double getPrecio() {
		double precioConDescuento;
		precioConDescuento = super.getPrecio()-((super.getPrecio()/100)* this.descuento);
		return precioConDescuento;
	}

}
