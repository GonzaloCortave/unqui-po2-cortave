package mercado;

public interface Registrable {
	Double registrarse();
}
