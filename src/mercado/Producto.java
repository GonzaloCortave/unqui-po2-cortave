package mercado;

public class Producto implements Registrable{
	private String nombre;
	private Double precio;
	private int stock;

	public Producto(String nombreP, double precioP, int cantidadStock) {
		nombre = nombreP;
		precio = precioP;
		stock = cantidadStock;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public Double registrarse() {
		this.decrementarStock();
		return this.getPrecio();
	}

	public void decrementarStock() {
		this.setStock(this.getStock()-1);
		
	}

}
