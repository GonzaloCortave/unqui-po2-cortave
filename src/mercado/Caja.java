package mercado;

public class Caja {
	private Double montoAPagar;
	
	public Caja() {
		montoAPagar = 0.0d;
	}

	public Double getMontoTotal() {
		double montoTotal = this.montoAPagar;
		this.montoAPagar = 0.0d;
		return montoTotal;
	}

	public void registrar(Registrable registrable) {
		this.montoAPagar = this.montoAPagar + registrable.registrarse();
		
	}
}
