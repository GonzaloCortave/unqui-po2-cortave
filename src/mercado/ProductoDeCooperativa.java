package mercado;

public class ProductoDeCooperativa extends Producto{

	public ProductoDeCooperativa(String nombreP, double precioP, int cantidadStock) {
		super(nombreP, precioP, cantidadStock);
	}
	
	@Override
	public Double getPrecio() {
		Double precioConDescuento = super.getPrecio()- ((super.getPrecio() / 100)*10);
		return precioConDescuento;
	}
}
