package impuesto;

public class IngresoHorasExtra extends Ingreso {
	
	private int horasExtra;
	
	
	public IngresoHorasExtra(String mesDeIngreso, String concepto, double monto, int unaCantDeHoras) {
		super(mesDeIngreso, concepto, monto);
		this.horasExtra = unaCantDeHoras;
		
	}

	public int getHorasExtra() {
		return horasExtra;
	}
	
	@Override
	public Double getMontoImponible() {
		return 0d;
	}
}
