package impuesto;

public class Ingreso {
	private String mesDePercepcion;
	private String concepto;
	private double montoPercibido;

	public Ingreso(String mesDeIngreso, String concepto, double monto) {
		this.mesDePercepcion =  mesDeIngreso;
		this.concepto = concepto;
		this.montoPercibido = monto;
	}

	public String getMesDePercepcion() {
		return mesDePercepcion;
	}

	public String getConcepto() {
		return concepto;
	}
	
	public Double getMontoPercibido() {
		return montoPercibido;
	}

	public Double getMontoImponible() {
		return this.getMontoPercibido();
	}
	public Double getImpuesto() {
		double impuesto = (this.getMontoImponible()/100) * 2;
		return impuesto;
	}
	

}

