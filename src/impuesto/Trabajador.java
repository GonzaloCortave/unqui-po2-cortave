package impuesto;

import java.util.ArrayList;

class Trabajador {
	
	private ArrayList<Ingreso> ingresos;
	
	public Trabajador() {
		ingresos = new ArrayList<Ingreso>();
	}

	public void agregarIngreso(Ingreso ingreso) {
		this.ingresos.add(ingreso);	
	}

	public ArrayList<Ingreso> getIngresos() {
		return ingresos;
	}

	public Double getTotalPercibido() {
		double totalPercibido=0d;
		for(Ingreso ingreso : this.ingresos) {
			totalPercibido = totalPercibido + ingreso.getMontoPercibido();
		}
		return totalPercibido;
	}

	public Double getMontoImponible() {
		double totalImponible=0d;
		for(Ingreso ingreso : this.ingresos) {
			totalImponible = totalImponible + ingreso.getMontoImponible();
		}
		return totalImponible;
	}

	public Double getImpuestoAPagar() {
		double impuestoAPagar = 0d;
		for (Ingreso ingreso : this.ingresos) {
			impuestoAPagar = impuestoAPagar + ingreso.getImpuesto();
		}
		return impuestoAPagar;
	}

}
